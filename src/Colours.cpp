#include "Colours.h"
#include <sstream>
#include <algorithm>

using namespace std;
using namespace colour;

Colours::Colours() {
    /* Populate the mapping array */
    _mapping["FG_BLACK"] = "\033[30m";
    _mapping["FG_RED"] = "\033[31m";
    _mapping["FG_GREEN"] = "\033[32m";
    _mapping["FG_YELLOW"] = "\033[33m";
    _mapping["FG_BLUE"] = "\033[34m";
    _mapping["FG_MAGENTA"] = "\033[35m";
    _mapping["FG_CYAN"] = "\033[36m";
    _mapping["FG_WHITE"] = "\033[37m";

    _mapping["BG_BLACK"] = "\033[40m";
    _mapping["BG_RED"] = "\033[41m";
    _mapping["BG_GREEN"] = "\033[42m";
    _mapping["BG_YELLOW"] = "\033[43m";
    _mapping["BG_BLUE"] = "\033[44m";
    _mapping["BG_MAGENTA"] = "\033[45m";
    _mapping["BG_CYAN"] = "\033[46m";
    _mapping["BG_WHITE"] = "\033[47m";

    _enabled = true;
}

Colours::~Colours() {
}

string Colours::reset() {
    return "\033[0m";
}

string Colours::getColour(const string &colour) {
    if (_enabled) {
        return _mapping[colour];
    } else {
        return "";
    }
}



string Colours::toUpper(const string &orig)  {
    string copy(orig);

    transform(copy.begin(), copy.end(), copy.begin(), ::toupper);
    return copy;
}

string Colours::fg(const string &source) {
    stringstream ss;
    ss << "FG_" << source;

    string upperString = toUpper(ss.str());
    //return _mapping[upperString];
    return getColour(upperString);
}

string Colours::bg(const string &source) {
    stringstream ss;
    ss << "BG_" << source;

    string upperString = toUpper(ss.str());
    //return _mapping[upperString];
    return getColour(upperString);
}

string Colours::fg(const string &col,
        const string &text) {

    stringstream ss;
    ss << fg(col) << text << reset();
    return ss.str();
}

string Colours::bg(const string &col,
        const string &text) {
    stringstream ss;
    ss << bg(col) << text << reset();
    return ss.str();
}

string Colours::error(const string &text) {
    return fg("red", text);
}

string Colours::ok(const string &text) {
    return fg("green", text);
}


void Colours::enabled(bool enabled) {
    _enabled = enabled;
}
