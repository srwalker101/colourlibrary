#ifndef COLOURS_H

#define COLOURS_H

#include <map>
#include <string>

namespace colour {

    class Colours {
        public:
            Colours();
            virtual ~Colours();

            std::string reset();

            std::string fg(const std::string &source);
            std::string bg(const std::string &source);
            std::string fg(const std::string &col, 
                    const std::string &text);
            std::string bg(const std::string &col, 
                    const std::string &text);

            std::string error(const std::string &text);
            std::string ok(const std::string &text);

            void enabled(bool enabled);




        private:

            std::string getColour(const std::string &colour);
            std::string toUpper(const std::string &orig);

            std::map<std::string, std::string> _mapping;
            bool _enabled;
    };


}

#endif /* end of include guard: COLOURS_H */
