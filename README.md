# Readme for project by Simon Walker

Colour printing library for command line applications


## Description

This program uses ascii codes to print terminal colours. Available
colours are:

* black
* red
* green
* yellow
* blue
* magenta
* cyan
* white

Both the background and foreground can be coloured seperately.

## Usage

Create a `Colours` class. It takes no construction parameters. One of
two methods to colour the text are as follows:

1. Use the one-parameter `fg`/`bg` methods:

        colour::Colours c;
        cout << c.fg("red") << "Hello world" << c.reset() << endl;

2. Use the two parameter methods which return the colour back to normal
   after the text is printed:

        colour::Colours c;
        cout << c.fg("red", "Hello world") << endl;

Also included are `error` and `ok` methods for printing red and green
text on the default background of the terminal. 

The `reset` method sets the defaults for the terminal again.

The class can be disabled at any point by calling the `enabled` method
with a boolean.




## Requirements

* c++ compiler
* stl



